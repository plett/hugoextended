FROM debian:10-slim

#install pygments for syntax hilighting
RUN apt-get -qq update \
    && DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends python-pygments git ca-certificates asciidoc curl \
    && rm -rf /var/lib/apt/lists/*

ENV HUGO_VERSION 0.101.0
ENV HUGO_BINARY hugo_extended_${HUGO_VERSION}_Linux-64bit.deb
ENV HUGO_SHA 9d479c2d185cfd1e49a8e9959c998f159a3a9d81280b2972b6a6a3235b88db54

RUN curl -sL -o /tmp/hugo.deb \
    https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} && \
    echo "${HUGO_SHA}  /tmp/hugo.deb" | sha256sum -c && \
    dpkg -i /tmp/hugo.deb && \
    rm /tmp/hugo.deb

WORKDIR /site

EXPOSE 1313

CMD /usr/local/bin/hugo
